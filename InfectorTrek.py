import numpy as np
import math
import tkinter as tk
import heapq as hq
from random import seed
from random import random

class Map:
    "A Class Containing The Map Locations As Children"
    def __init__(self,maxI, maxJ, globalSeed):
        self.seed = globalSeed
        self.locations = []
        self.maxI = maxI
        self.maxJ = maxJ
        self.iteration = 0

        hq.heapify(self.locations) 

        for y in range(maxJ):
            for x in range(maxI):
                label = x+(y*maxI)
                hq.heappush(self.locations, Square(label, x, y, self.seed))
                
    def initialInfect(self, initPercentInfected): # initial infect is functionally different to iterating infect as x% of squares being infected is different to each square having x% chance to be infected
        numberOfLocations = (self.maxI*self.maxJ)
        numberOfInfections = math.trunc(numberOfLocations*initPercentInfected)
        seed(self.seed)

        if numberOfInfections > numberOfLocations-2:
            numberOfInfections = numberOfLocations-2 # accounts for a very high percentage that would require the first or last square to be used, 

        n = 0
        while n < numberOfInfections:
            infectingSquare = math.trunc(1+(random()*(numberOfLocations-2))) # Not possible to infect first or last square on init otherwise it is unsolvable
            if self.locations[infectingSquare].infection == False:
                self.locations[infectingSquare].infection = True
                n += 1

    def iteratingInfect(self, percentChanceToInfect):
        numberOfLocations = len(self.locations)
        self.iteration += 1
        seed(self.seed+(numberOfLocations*self.iteration))

        for n in range(numberOfLocations):
            if self.locations[n].hasSneezed == False and self.locations[n].infection == True and random() < percentChanceToInfect:

                self.locations[n].hasSneezed = True
                infectorI = self.locations[n].i
                infectorJ = self.locations[n].j

                infecteeAbove = infectorJ+1
                infecteeLeft = infectorI-1
                infecteeRight = infectorI+1
                infecteeBelow = infectorJ-1

                # these four infect the nearby squares checking that they exist and are not infected
                if infecteeAbove < self.maxJ:
                    infecteeI = infectorI
                    infecteeJ = infecteeAbove
                    label = infecteeI+(infecteeJ*self.maxI)
                    if self.locations[label].infection == False:
                        self.locations[label].sneezedOn = True

                if infecteeLeft >= 0:
                    infecteeI = infecteeLeft
                    infecteeJ = infectorJ
                    label = infecteeI+(infecteeJ*self.maxI)
                    if self.locations[label].infection == False:
                        self.locations[label].sneezedOn = True

                if infecteeRight < self.maxI:
                    infecteeI = infecteeRight
                    infecteeJ = infectorJ
                    label = infecteeI+(infecteeJ*self.maxI)
                    if self.locations[label].infection == False:
                        self.locations[label].sneezedOn = True
                
                if infecteeBelow >= 0:
                    infecteeI = infectorI
                    infecteeJ = infecteeBelow
                    label = infecteeI+(infecteeJ*self.maxI)
                    if self.locations[label].infection == False:
                        self.locations[label].sneezedOn = True
                    

        # this stops one iteration from using newly infected cells in the infection process
        for n in range(numberOfLocations):
            if self.locations[n].sneezedOn == True:
                self.locations[n].sneezedOn = False
                self.locations[n].infection = True          

class Square:
    "A Location on the Map"
    def __init__(self,label, i, j, globalSeed):
        self.seed = globalSeed
        self.label = label
        self.i = i
        self.j = j
        self.infection = False
        self.sneezedOn = False
        self.hasSneezed = False

    # defining less than for purposes of heap queue
    def __lt__(self, other):
      return self.label < other.label
    
    # defining greater than for purposes of heap queue
    def __gt__(self, other):
      return self.label > other.label


class Node():
    """A node class for Pathfinding"""
    # Based on information related to REF https://medium.com/@nicholas.w.swift/easy-a-star-pathfinding-7e6689c7f7b2

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        # Dijkstra's Info
        self.distance = math.inf

        # A* Info
        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position

    def __repr__(self):
      return f"{self.position} - g: {self.g} h: {self.h} f: {self.f}"

    # defining less than for purposes of heap queue
    def __lt__(self, other):
      return self.f < other.f
    
    # defining greater than for purposes of heap queue
    def __gt__(self, other):
      return self.f > other.f

def returnPath(currentNode):
    path = []
    current = currentNode
    while current is not None:
        path.append(current.position)
        current = current.parent
    return path[::-1]  # Return reversed path 

def dijkstra(world):
    """Returns a list of tuples as a path from the given start to the given end in the given world as the shortest distance"""
    # Based on pseudocode https://medium.com/@nicholas.w.swift/easy-dijkstras-pathfinding-324a51eeb0f
    i = world.maxI
    j = world.maxJ
    if world.locations[0].infection or world.locations[i-1+((j-1)*i)].infection: # Find if the start or end is blocked with infection
        return None


    # Initialize both open and closed list
    unexploredList = []
    exploredList = []

    # Heapify the lists and Add the start node
    hq.heapify(unexploredList) 
    hq.heapify(exploredList) 
            
    # Create start and end node
    startNode = Node(None, (0,0))
    endNode = Node(None, (i-1,j-1))
    startNode.distance = 0
    hq.heappush(unexploredList, (startNode.distance, startNode))


    while len(unexploredList) > 0:
        currentNodePackage = hq.heappop(unexploredList) # Get node with shortest distance

        currentNode = currentNodePackage[1]

        hq.heappush(exploredList, currentNode)

        # Found the goal
        if currentNode == endNode:

            return returnPath(currentNode)

        for newPosition in [(0, -1), (0, 1), (-1, 0), (1, 0)]: # Adjacent squares

            # Get node position
            nodePosition = (currentNode.position[0] + newPosition[0], currentNode.position[1] + newPosition[1])
            

            # Make sure within range
            if nodePosition[0] > (i - 1) or nodePosition[0] < 0 or nodePosition[1] > (j-1) or nodePosition[1] < 0:
                continue

            
            # Make sure not infected
            label = nodePosition[0]+(nodePosition[1]*i)
            if world.locations[label].infection == True:
                continue

            # Create new node
            newNode = Node(currentNode, nodePosition)
            newNode.distance = currentNode.distance + 1

            # Not traversed Before
            if len([exploredNode for exploredNode in exploredList if exploredNode == newNode]) > 0:
                continue

            # Not currently to be explored 
            if len([unexploredPackage for unexploredPackage in unexploredList if unexploredPackage[1] == newNode]) > 0:
                continue

            hq.heappush(unexploredList, (newNode.distance, newNode)) # Saved with distance, as the heap sorts tuples automatically by their [0]

    return None

def astar(world):
    """Returns a list of tuples as a path from the given start to the given end in the given maze"""
    # Based on information related to REF https://medium.com/@nicholas.w.swift/easy-a-star-pathfinding-7e6689c7f7b2

    i = world.maxI
    j = world.maxJ
    if world.locations[0].infection or world.locations[i-1+((j-1)*i)].infection: # Find if the start or end is blocked with infection
        return None

    # Create start and end node
    startNode = Node(None, (0,0))
    startNode.g = startNode.h = startNode.f = 0
    endNode = Node(None, (i-1,j-1))
    endNode.g = endNode.h = endNode.f = 0

    # Initialize both open and closed list
    openList = []
    closedList = []

    # Heapify the open_list and Add the start node
    hq.heapify(openList) 
    hq.heappush(openList, startNode)


    #Stop Condition
    outerIterations = 0
    maxIterations = (i*j)+1


    # Loop until you find the end
    while len(openList) > 0:
        outerIterations += 1

        
        # Get the current node
        currentNode = hq.heappop(openList)
        closedList.append(currentNode)

        #Time out function if map is non-traversable and too complex
        if outerIterations > maxIterations:
          return None       


        # Found the goal
        if currentNode == endNode:
            return returnPath(currentNode)

        # Generate children
        children = []

        for newPosition in [(0, -1), (0, 1), (-1, 0), (1, 0)]: # Adjacent squares

            # Get node position
            nodePosition = (currentNode.position[0] + newPosition[0], currentNode.position[1] + newPosition[1])

            # Make sure within range
            if nodePosition[0] > (i - 1) or nodePosition[0] < 0 or nodePosition[1] > (j-1) or nodePosition[1] < 0:
                continue

            # Make sure non-infected mode
            label = nodePosition[0]+(nodePosition[1]*i)
            if world.locations[label].infection == True:
                continue
   
            # Create new node
            newNode = Node(currentNode, nodePosition)

            # Append
            children.append(newNode)

        # Loop through children
        for child in children:

            # Child is on the closed list
            if len([closedChild for closedChild in closedList if closedChild == child]) > 0:
                continue

            # Create the f, g, and h values
            child.g = (currentNode.g + 1)
            child.h = ((child.position[0] - endNode.position[0]) ** 2) + ((child.position[1] - endNode.position[1]) ** 2)
            child.f = child.g + child.h

            # Child is already in the open list
            if len([openNode for openNode in openList if child.position == openNode.position and child.g > openNode.g]) > 0:
                continue

            # Add the child to the open list
            hq.heappush(openList, child)
        
    return None


class AgerrisGui(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.createInputs()

    
    def createInputs(self):
        master = self.master

        canvasSize = (1200, 500) # change to fit application to your screen size

        # Title
        title = tk.Text(master,bg="#ececec", fg="#3b3433", height=1, width=12, font=('Times',42))
        title.insert(tk.END, " Infector Trek")
        title.config(state=tk.DISABLED)

        title.grid(row=0, column=0)

        # Entry Labels
        tk.Label(master, text="Columns *").grid(row=2, column=0)
        tk.Label(master, text="Rows *").grid(row=3, column=0)
        tk.Label(master, text="Initial Infection Rate (%) *").grid(row=2, column=2)
        tk.Label(master, text="Infection Rate Per Iteration (%) *").grid(row=3, column=2)
        tk.Label(master, text="Seed (number)").grid(row=4)

        # Entry entry boxes
        columns = tk.Entry(master, width=32)
        rows = tk.Entry(master, width=32 )
        initInfect = tk.Scale(master, length=300, from_=0, to=100, orient=tk.HORIZONTAL)
        iterInfect = tk.Scale(master, length=300, from_=0, to=100, orient=tk.HORIZONTAL)
        seed = tk.Entry(master, width=32)
        
        columns.grid(row=2, column=1)
        rows.grid(row=3, column=1)
        initInfect.grid(row=2, column=3)
        iterInfect.grid(row=3, column=3)
        seed.grid(row=4, column=1)

        # Log box
        commsBox = tk.Text(master,bg="#FAFAFA", fg="#3b3433",padx=12, height=4, width=80, font=('Times',12))
        commsBox.insert(tk.INSERT, "[Log Starts]\n")
        commsBox.grid(row=4, column=2, columnspan=2, rowspan=2)
        

        def paintWorldGrid():
            # paints black lines with reference to the chosen canvas size

            # resets canvas to stop graphical slowdown
            self.window= tk.Canvas(master, bd=0, bg= "#FAFAFA", width=canvasSize[0], height=canvasSize[1])
            self.window.grid(row=1, 
                                                       column=0,
                                                       columnspan=6, 
                                                       pady=4)
            numColumns = int(columns.get())
            numRows = int(rows.get())
            sizeColumns = canvasSize[0]/numColumns
            sizeRows = canvasSize[1]/numRows

            # Vertical Lines
            numVertLines = numColumns-1

            for n in range(numVertLines):
                mathN = (n+1)
                self.window.create_line(mathN*sizeColumns, 0, mathN*sizeColumns, canvasSize[1], fill="#000000", width=1)

            # Horizontal Lines
            numHorzLines = numRows-1
            for n in range(numHorzLines):
                mathN = (n+1)
                self.window.create_line(0, mathN*sizeRows, canvasSize[0], mathN*sizeRows, fill="#000000", width=1)

        def paintWorldInfections():
            # paints squares green where there are infections
            paintWorldGrid()
            world = self.worldMap

            numColumns = int(columns.get())
            numRows = int(rows.get())

            numberLocations = numColumns*numRows

            sizeColumns = canvasSize[0]/numColumns
            sizeRows = canvasSize[1]/numRows
            
            for label in range(numberLocations):
                if world.locations[label].infection:
                    infectionLocation = (world.locations[label].i,world.locations[label].j)

                    self.window.create_rectangle(infectionLocation[1]*sizeColumns, 
                                                    infectionLocation[0]*sizeRows, 
                                                    (infectionLocation[1]+1)*sizeColumns, 
                                                    (infectionLocation[0]+1)*sizeRows, 
                                                    fill="#9bd12e")


        def initWorld():
            # initialises a Map class with the given Specs and checks if those specs make sense
            try:
                worldSeed = float(seed.get())
            except:
                worldSeed = random()*1000
                pass

            try:
                worldI = int(rows.get())
                worldJ = int(columns.get())
                worldInitInf = float(initInfect.get()/100)
                float(iterInfect.get()/100)
            except:
                commsBox.insert(tk.INSERT, "These Specifications Cannot Be Read\n")
                commsBox.see(tk.END)
                return
            
            if worldI == 0 or worldJ == 0:
                commsBox.insert(tk.INSERT, "The World Cannot Have Zero Area\n")
                commsBox.see(tk.END)
                return 

            # cleans the log box
            commsBox.delete('1.0', tk.END)
            commsBox.insert(tk.INSERT, "[Log Starts]\n")

            # Maths out the current state of the world
            self.worldMap = Map(worldI, worldJ, worldSeed)
            self.worldMap.initialInfect(worldInitInf)

            #paints the current state of the world
            paintWorldGrid()
            
            paintWorldInfections()
            

        def DjikWorld():
            # paints a orange path where the Djikstra algorithm finds the fastest path

            # Check if the world has been made
            try:
                self.worldMap
            except:
                commsBox.insert(tk.INSERT, "Create A World First\n")
                commsBox.see(tk.END)
                return

            numColumns = int(columns.get())
            numRows = int(rows.get())

            sizeColumns = canvasSize[0]/numColumns
            sizeRows = canvasSize[1]/numRows

            path = dijkstra(self.worldMap) # get steps
            if path == None:
                commsBox.insert(tk.INSERT, "This Environment Is Not Traversable\n")
                commsBox.see(tk.END)
            else:
                for step in path: # Paint steaps
                    self.window.create_rectangle(step[1]*sizeColumns, step[0]*sizeRows, (step[1]+1)*sizeColumns, (step[0]+1)*sizeRows, fill="#ffa500")

        def AStarWorld():
            # paints a purple path where the A* algorithm finds the fastest path

            # Check if the world has been made
            try:
                self.worldMap
            except:
                commsBox.insert(tk.INSERT, "Create A World First\n")
                commsBox.see(tk.END)
                return

            #paintWorldInfections()
            numColumns = int(columns.get())
            numRows = int(rows.get())

            sizeColumns = canvasSize[0]/numColumns
            sizeRows = canvasSize[1]/numRows

            path = astar(self.worldMap) # gets steps
            if path == None:
                commsBox.insert(tk.INSERT, "This Environment Is Not Traversable\n")
                commsBox.see(tk.END)
            else:
                for step in path: # paint steps
                    self.window.create_rectangle(step[1]*sizeColumns, step[0]*sizeRows, (step[1]+1)*sizeColumns, (step[0]+1)*sizeRows, fill="#8d70ff")


        def iterWorld():
            # iterates the world infection
            try:
                self.worldMap
            except:
                commsBox.insert(tk.INSERT, "Create A World First\n")
                commsBox.see(tk.END)
                return

            
            worldIterInf = float(iterInfect.get()/100)
            self.worldMap.iteratingInfect(worldIterInf)

            commsBox.insert(tk.INSERT, f'Iteration Number [{self.worldMap.iteration}]\n')
            commsBox.see(tk.END)
            
            # updates world with new infections
            paintWorldInfections()

                

        # Quit Button
        tk.Button(master, 
            text='Quit', 
            height=2,
            width =5,
            command=master.quit).grid(row=5, 
                                    column=0, 
                                    sticky=tk.W, 
                                    pady=4)
        # Build Button
        tk.Button(master, 
            text='Build World', 
            height=2,
            width =5,
            command=initWorld).grid(row=5, 
                                                       column=0, 
                                                       sticky=tk.E, 
                                                       pady=4)
        # Quit Button
        tk.Button(master, 
            text='Iterate World', 
            height=2,
            width =6,
            command=iterWorld).grid(row=5, 
                                                       column=1, 
                                                       sticky=tk.W, 
                                                       pady=4)
        # Djik Button                                    
        tk.Button(master, 
            text='Fast Track', 
            height=2,
            width =6,
            command=DjikWorld).grid(row=5, 
                                                       column=1, 
                                                       sticky=tk.S, 
                                                       pady=4)
        # A* Button
        tk.Button(master, 
            text='Cheap Track', 
            height=2,
            width =6,
            command=AStarWorld).grid(row=5, 
                                                       column=1, 
                                                       sticky=tk.E, 
                                                       pady=4)

                                                       
        # used to init window
        window= tk.Canvas(master, bd=0, bg= "#FAFAFA", width=canvasSize[0], height=canvasSize[1])
        window.grid(row=1, 
                                                       column=0,
                                                       columnspan=8, 
                                                       pady=4)


root = tk.Tk()
app = AgerrisGui(master=root)
app.mainloop()

